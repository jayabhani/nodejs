const express = require('express')
const helmet = require('helmet')
const app = express()

app.use(helmet())

app.get("/", (req, res) => {
    res.send('Learning helmet module')
})

app.listen(3000, (err) => {
    if(err)
        console.log('err');
    else
        console.log('server started at hppp://localhost:3000');
})