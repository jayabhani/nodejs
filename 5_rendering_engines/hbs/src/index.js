const express = require('express');
const app = express();
app.set("view engine", "hbs");

// app.get("", (req, res) => {
//     res.render("index");
// });


var demo = {
    name : 'John',
    age : 26
}
  
// app.get('/', (req, res)=>{
//      res.render('index', {demo : demo})
// })


var projects = {
    name : 'John', 
    skills : ['Data Mining', 'BlockChain Dev', 'node.js']
}
  
app.get('/', (req, res)=>{
    res.render('index', {projects : projects});
})

var server = app.listen(4000, function(){
    console.log('listening to port 4000')
});