const express = require('express');
var app = express();

app.set('view engine', 'ejs');

app.get('/', (req, res) => {
    // res.render('home', {name: "jay"});


    var data = {name:'John',
    hobbies:['playing football', 'playing chess', 'cycling']}
 
    res.render('home', {data:data});
});

// app.listen(3000, () => {
//     console.log("Server is up on port 3000");
// });

var server = app.listen(4000, function(){
    console.log('listening to port 4000')
});