const express = require('express')
const cors = require('cors')
const app = express()
const port = 3000

// app.get('/', (req, res) => {
//     res.send('CORS with Node.js and Express')
// })

// app.listen(port, () => {
//     console.log(`app listening on port ${3000}`);
// })

app.use(cors({
    // origin: ['https://www.google.com', 'https:www.facebook.com']
    origin: '*'
}))

// app.use(cors())

const person = [
    {
        "id": "1",
        "name": "jay"
    },
    {
        "id": "2",
        "name": "ravi"
    },
    {
        "id": "3",
        "name": "ajay"
    },
    {
        "id": "4",
        "name": "raj"
    }
]

app.get('/person', (req, res) => {
    res.send(person)
})

app.listen(port)

// cross-origin is allowed when you’re currently on the same domain, and you are executing this request from the same domain.


// now let's try to get the ingredients using the fetch command from another site instead

// fetch("http://localhost:3000/person").then(req => req.text()).then(console.log)

