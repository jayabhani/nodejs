// console.log('starting');

// setTimeout(() => {
//     console.log('2 second timer');
// }, 2000);

// setTimeout(() => {
//     console.log('0 second timer');
// }, 0);
 
// console.log('stopping');


// examples to understand asynchronous nodejs
// call stack
// node apis
// callback queue
// event loop

// example-1
// const x = 1;
// const y = x + 2;
// console.log('sum = ' + y);

// example-2
// const listLocations = (locations) => {
//     locations.forEach((location) => {
//         console.log(location);
//     });
// }

// const myLocations = ["US", "UK"];

// listLocations(myLocations);

// example-3
console.log('starting');

setTimeout(() => {
    console.log('2 second timer');
}, 2000);

setTimeout(() => {
    console.log('0 second timer');
}, 0);
 
console.log('stopping');