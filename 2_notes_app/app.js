// nodejs module system
// const fs = require('fs');

// fs.writeFileSync('notes.txt', 'This file is created by NodeJS again! ');


// challenge: append a message to notes.txt
// fs.appendFileSync('notes.txt', 'This is an appended message in this file.');




// require('./utils.js');
// const myVar = 'demo';
// console.log(myVar);


// const fName = require('./utils.js');
// console.log(fName);

// const addition = require('./utils.js');
// console.log(addition(2, 3));

// const myName = require('./utils.js');
// console.log(myName('jay', 'abhani'));


// challenge: define and use a function in a new line
// const getNotes = require('./notes.js');
// const msg = getNotes();
// console.log(msg);




// file system and command line args
// npm init
// npm i validator@13.7.0
// npm install


// starting actual project along with concepts

// const getNotes = require('./notes.js');
// const validator = require('validator');
// const chalk = require('chalk');
const yargs = require('yargs');

// console.log(getNotes())

// npm install validator
// console.log(validator.isEmail('example.com'));
// console.log(validator.isEmail('example@gmail.com'));

// console.log(validator.isURL('https://google.com'));


// challenge: use chalk library in your project
// npm install chalk@2.4.1
// console.log(chalk.green('Success!'));
// console.log(chalk.green.bold('Success!'));
// console.log(chalk.bold.green('Success!'));
// console.log(chalk.inverse.red('Error!'));


// npm install nodemon@1.18.5 -g
// nodemon -v
// nodemon app.js





// file system and command line arguments
// console.log(process.argv);
// console.log(process.argv[2]);
// node app.js jay
// node app.js add
// node app.js remove
// node app.js jay

// const command = process.argv[2];
// if(command === 'add'){
//     console.log('adding notes...');
// }else if(command === 'remove'){
//     console.log('removing notes...');
// }


// node app.js add --title="this is my title"
// console.log(process.argv);

// node app.js
// console.log(yargs.argv);

// node app.js add --title="things to buy"
// node app.js --help
// node app.js --version    //1.0.0

// yargs.version('1.1.0');
// console.log(yargs.argv);

// creating add command
// yargs.command({
//     command: 'add',
//     describe: 'add a new note',
//     handler: function(){
//         console.log('adding a new note!');
//     }
// });
// console.log(yargs.argv);
// node app.js --help
// node app.js add


// creating remove command
// yargs.command({
//     command: 'remove',
//     describe: 'remove a note',
//     handler: function(){
//         console.log('removing the note');
//     }
// })
// console.log(yargs.argv);
// node app.js --help
// node app.js remove


// challenge: add two new commands
// creating list xommand
// yargs.command({
//     command: 'list',
//     describe: 'list your notes',
//     handler: function(){
//         console.log('listing out all notes');
//     }
// })
// console.log(yargs.argv);
// node app.js list

// creating read command
// yargs.command({
//     command: 'read',
//     describe: 'read a note',
//     handler: function(){
//         console.log('reading a note');
//     }
// })
// yargs.argv;
// console.log(yargs.argv);
// node app.js read


// yargs.command({
//     command: 'add',
//     describe: 'add a new note',
//     builder: {          //options
//         title: {
//             describe: 'note title',
            // demandOption: true,
            // type: 'string'
    //     },
    //     body: {
    //         describe: 'note body',
    //         demandOption: true,
    //         type: 'string'
    //     }
    // },
    // handler: function(argv){
        // console.log('adding a new note', argv);
//         console.log('Title: ' + argv.title);
//         console.log('Body: ' + argv.body);
//     }
// });
// yargs.argv;
// yargs.parse();
// node app.js add --title-"shopping list"
// node app.js add --title
// node app.js add


// challenge: add an option to yargs









