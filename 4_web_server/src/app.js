const express = require("express");
const app = express();

// app.com
// app.get('', (req, res) => {
//     res.send("Hello Express");
// });

// app.com/about
// app.get('/about', (req, res) => {
//     res.send("This is about app");
// });

// app.com/help
// app.get('/help', (req, res) => {
//     res.send('<h1>Help Page</h1>');
// });

app.get("/help", (req, res) => {
  // res.send({
  //     name: 'jay',
  //     age: 22
  // });
  res.send([
    {
      name: "jay",
    },
    {
      name: "ajay",
    },
  ]);
});

app.listen(3000, () => {
  console.log("Server is up on port 3000");
});


/*
1750
150
350
1200

3450
9000
*/