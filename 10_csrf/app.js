
const express = require('express');
const cookieParser = require('cookie-parser');
const csrf = require('csurf');
var bodyParser = require('body-parser');

csrfProtection = csrf({ cookie: true });
const app = express()
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/transfer", csrfProtection, (req, res) => {

    res.send(`
    <html>
    <form id="myForm" action="/transfer" method="POST" target="_blank">
    Account:<input type="text" name="account" value="your friend"/><br/>
    Amount:<input type="text" name="amount" value="$5000"/>
    <input type="hidden" name="_csrf" value="${req.csrfToken()}"/>
      <button type="submit">Transfer Money</button>
    </form>
    </html>
    `)
});

app.post("/transfer", csrfProtection, (req, res) => {
    console.log(req.body)
    if (isAuthenticated(req.cookies["session"])) {
        // Transfer money and insert data in the database
        console.info("Transferring Money...")
        res.send("OK")
    } else {
        res.status(400).send("Bad Request")
    }
});

app.listen(3000);

const isAuthenticated = (session) => {
    // We should check session in a store or something like that
    return (session === "valid_user")
}